<?php

/**
 * @file
 * Contains \Drupal\google_calendar_api\Controller\MainController.
 */

namespace Drupal\google_calendar_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Form\FormBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Datetime\DrupalDateTime;

use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;

class GoogleCalendarAPIController extends ControllerBase {
  const sss = 'ssss';
  const OAUTH_ENDPOINT = 'https://accounts.google.com/o/oauth2/';

  const OAUTH_AUTHORIZATION = 'v2/auth';
  const OAUTH_ACCESS_TOKEN = 'token';

  const GRANT_TYPE_AUTH_CODE = 'authorization_code';
  const GRANT_TYPE_REFRESH_TOKEN = 'refresh_token';

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilder
   */
  protected $formBuilder;

  /**
   * The GoogleCalendarAPIController constructor.
   *
   * @param \Drupal\Core\Form\FormBuilder $formBuilder
   *   The form builder.
   */
  public function __construct(FormBuilder $formBuilder) {
    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder')
    );
  }

  public function getAuthorizeClientURL($client_id, $callback_url) {
    $scope = "https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/calendar.readonly https://www.googleapis.com/auth/calendar.events https://www.googleapis.com/auth/userinfo.profile";

    $options = ['absolute' => TRUE];
    $query_params = array(
      'response_type' => 'code',
      'client_id' => $client_id,
      'redirect_uri' => $callback_url,
      'scope' => $scope,
      'prompt' => 'consent',
      'access_type' => 'offline',
      'state' => base64_encode($callback_url),
    );
    
    $current_uri = \Drupal::request()->getSchemeAndHttpHost() . \Drupal::request()->getRequestUri();
    $forward_url = self::OAUTH_ENDPOINT . self::OAUTH_AUTHORIZATION;
    $forward_url = Url::fromRoute('<current>', $query_params, $options);

    $str = str_replace($current_uri, self::OAUTH_ENDPOINT . self::OAUTH_AUTHORIZATION, $forward_url->toString());

    return $str;
  }

  public function getOAuth2CallbackUrl(){
    $access_token = $this->getAccessToken();

    $build['#attached']['library'][] = 'google_calendar_api/form-script-google-calendar-api-callback';
    return $build;
  }

  public function checkValidToken(){
    $google_calendar_api_access_token = \Drupal::state()->get('google_calendar_api_access_token');
    
    try{
      $response = \Drupal::httpClient()->get('https://www.googleapis.com/oauth2/v2/userinfo?alt=json&access_token='.$google_calendar_api_access_token);
      return TRUE;
      //return $response->getBody()->getContents();

    }catch(\Exception $e){
      $refresh_token = $this->refreshToken();
      return FALSE;
      //return $e->getMessage();
    }
  }

  private function getAccessToken(){
    $config = \Drupal::config('google_calendar_api.settings');
    $client_id = $config->get('google_calendar_api_client_id');
    $secret = $config->get('google_calendar_api_secret');

    $scope = "https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/calendar.readonly https://www.googleapis.com/auth/calendar.events https://www.googleapis.com/auth/userinfo.profile";

    $query_params = array(
      'client_id' => $client_id,
      'client_secret' => $secret,
      'code' => $_GET['code'],
      'grant_type' => 'authorization_code',
      'redirect_uri' => base64_decode($_GET['state']),
      'scope' => $scope,
      'acess_type' => 'offline',
    );

    $options = array(
      'verify' => TRUE,
      'form_params' => $query_params,
      'timeout' => 30,
      'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
    );

    $response = \Drupal::httpClient()->post('https://accounts.google.com/o/oauth2/token', $options);
    $contents = Json::decode($response->getBody()->getContents());

    $googleCalendarContents = array(
      'google_calendar_api_access_token' => $contents['access_token'],
      'google_calendar_api_expires_in' => $contents['expires_in'],
      'google_calendar_api_refresh_token' => $contents['refresh_token'],
      'google_calendar_api_token_type' => $contents['token_type']
    );
    \Drupal::state()->setMultiple($googleCalendarContents);

    #$access_token = \Drupal::state()->set('google_calendar_api_access_token', $contents['access_token']);
    #$expires_in = \Drupal::state()->set('google_calendar_api_expires_in', $contents['expires_in']);
    #$refresh_token = \Drupal::state()->set('google_calendar_api_refresh_token', $contents['refresh_token']);
    #$token_type = \Drupal::state()->set('google_calendar_api_token_type', $contents['token_type']);

    return $contents['access_token'];
  }

  private function refreshToken(){
    $config = \Drupal::config('google_calendar_api.settings');
    $client_id = $config->get('google_calendar_api_client_id');
    $secret = $config->get('google_calendar_api_secret');
    $refresh_token = \Drupal::state()->get('google_calendar_api_refresh_token');

    $query_params = array(
      'client_id' => $client_id,
      'client_secret' => $secret,
      'grant_type' => 'refresh_token',
      'refresh_token' => $refresh_token,
      'acess_type' => 'offline',
    );
    
    $request_access_token_url = 'https://www.googleapis.com/oauth2/v4/token';//

    $options = array(
      'verify' => TRUE,
      'form_params' => $query_params,
      'timeout' => 30,
      'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
    );
    $response = \Drupal::httpClient()->post($request_access_token_url, $options);
    $contents = Json::decode($response->getBody()->getContents());

    $access_token = \Drupal::state()->set('google_calendar_api_access_token', $contents['access_token']);

    return $contents;
  }

  public function testAPI(){
    $refresh_token = $this->refreshToken();
    $build = array();
    return $build;
  }

  public function newtest(){
    return 'newtest';
  }
}
