<?php

namespace Drupal\google_calendar_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\google_calendar_api\Controller\GoogleCalendarAPIController;

/**
 * Defines a form that configures forms module settings.
 */
class GoogleCalendarApiConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'google_calendar_api_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'google_calendar_api.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    #error_log('adasdasd');
    #error_reporting('azczczxc');
    syslog(LOG_WARNING, 'messageaaaa111');

    $access_token = \Drupal::state()->get('google_calendar_api_access_token');
    $expires_in = \Drupal::state()->get('google_calendar_api_expires_in');
    $refresh_token = \Drupal::state()->get('google_calendar_api_refresh_token');
    $token_type = \Drupal::state()->get('google_calendar_api_token_type');
    dpm($access_token);
    dpm($expires_in);
    dpm($refresh_token);
    dpm($token_type);

    $config = $this->config('google_calendar_api.settings');
    $authorizeClientURL = GoogleCalendarAPIController::getAuthorizeClientURL($config->get('google_calendar_api_client_id'), \Drupal::request()->getSchemeAndHttpHost() . '/google-calendar-api-oauth2-callback');

    $form['google_calendar_api_client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => $config->get('google_calendar_api_client_id'),
    ];

    $form['google_calendar_api_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#default_value' => $config->get('google_calendar_api_secret'),
    ];

    $form['google_calendar_api_callback_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Callback URL'),
      '#attributes' => array('readonly' => 'readonly'),
      '#default_value' => \Drupal::request()->getSchemeAndHttpHost() . '/google-calendar-api-oauth2-callback',
    ];

    if (!isset($access_token)){
      $form['actions']['submit_button_2'] = array(
        '#type' => 'button',
        '#value' => t('Connect to Google Calendar'),
        '#attributes' => array(
            'class' => array(
                'google-calendar-api-oauth2-connect-link'
            )
        )
      );
    }else{
      #$button = array(
      #    '#type' => 'button',
          //'#submit' => array(
          //    'hr_google_calendar_oauth2_connect_button_disconnect_submit_form'
          //),
          //'#executes_submit_callback' => TRUE,
      #    '#value' => t('Disconnect from Google Calendar'),
      #    '#attributes' => array(
      #        'class' => array(
      #            'btn-danger'
      #        )
      #    ),
      #    '#weight' => 9999
      #);

      #$form['actions']['disconnect_button'] = $button;
      $form['actions']['disconnect_button'] = array(
        '#type' => 'submit',
        '#value' => t('Disconnect from Google Calendar'),
        '#submit' => array([$this, 'disconnectGoogleCalendar']),
      );
    }

    $form['#attached']['library'][] = 'google_calendar_api/form-script-google-calendar-api-config-form';
    $form['#attached']['drupalSettings']['google_calendar_api_config_form']['callbackUrl'] = $authorizeClientURL;
    dpm($authorizeClientURL);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('google_calendar_api.settings')
      ->set('google_calendar_api_client_id', $values['google_calendar_api_client_id'])
      ->set('google_calendar_api_secret', $values['google_calendar_api_secret'])
      ->set('google_calendar_api_callback_url', $values['google_calendar_api_callback_url'])
      ->save();
    parent::submitForm($form, $form_state);
  }

  public function disconnectGoogleCalendar(array &$form, FormStateInterface $form_state) {
    $access_token = \Drupal::state()->delete('google_calendar_api_access_token');
    $expires_in = \Drupal::state()->delete('google_calendar_api_expires_in');
    $refresh_token = \Drupal::state()->delete('google_calendar_api_refresh_token');
    $token_type = \Drupal::state()->delete('google_calendar_api_token_type');

    drupal_set_message('Disconnected from Google Calendar', 'warning');
  }
}