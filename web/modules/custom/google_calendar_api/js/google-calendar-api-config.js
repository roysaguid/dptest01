(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.callbackUrl = {
    attach: function attach(context) {
      $('.google-calendar-api-oauth2-connect-link').unbind('click').on('click', function(){
          window.open(drupalSettings.google_calendar_api_config_form.callbackUrl, '_blank', 'width=500,height=700');
          return false;
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
