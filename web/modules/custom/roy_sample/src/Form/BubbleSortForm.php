<?php
/**
 * @file
 * Contains \Drupal\roy_sample\Form\BubbleSortForm.
 */

namespace Drupal\roy_sample\Form;

#use Drupal\Core\Controller\ControllerBase;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Url;
#use Drupal\Core\Controller\ControllerBase;

class BubbleSortForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bubblesortform';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = array();
    dpm('asdasd23232');

    $numbers = array(1,3,2,5,2);
    $array_size = count($numbers);

    echo "Numbers before sort: ";
    for ( $i = 0; $i < $array_size; $i++ )
       dpm($numbers[$i]);
    echo "n";

    for ( $i = 0; $i < $array_size; $i++ )
    {
       for ($j = 0; $j < $array_size; $j++ )
       {
          if ($numbers[$i] < $numbers[$j])
          {
              $temp = $numbers[$i];
              $numbers[$i] = $numbers[$j];
              $numbers[$j] = $temp;
          }
       }
    }

    dpm("Numbers after sort: ");
    for( $i = 0; $i < $array_size; $i++ )
        dpm($numbers[$i]);
    echo "n";

    return $form;
  }

  /**
   * {@inheritdoc}
   */
    public function validateForm(array &$form, FormStateInterface $form_state) {
      #if (strlen($form_state->getValue('candidate_number')) < 10) {
      #  $form_state->setErrorByName('candidate_number', $this->t('Mobile number is too short.'));
      #}

    }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state_values = $form_state->getValues();
   }

}
